import pickle
import numpy as np
import matplotlib.pyplot as plt
with open('./2018-05-25-19-19-51.pickle', 'rb') as f:
    x = pickle.load(f)
with open('./2018-05-25-18-56-44.pickle', 'rb') as g:
    y = pickle.load(g)
with open('./2018-05-25-19-19-51.pickle', 'rb') as h:
    z = pickle.load(h)


data = np.array(x)

trainRecord = data[0,:,:]
evalRecord = data[1,:,:]
testRecord = data[2,:,:]
train_lst = trainRecord[:,0]
eval_lst = evalRecord[:,0]
test_lst = testRecord[:,0]

data2 = np.array(y)

trainRecord2 = data2[0,:,:]
evalRecord2 = data2[1,:,:]
testRecord2 = data2[2,:,:]
train_lst2 = trainRecord2[:,0]
eval_lst2 = evalRecord2[:,0]
test_lst2 = testRecord2[:,0]

data3 = np.array(z)

trainRecord3 = data3[0,:,:]
evalRecord3 = data3[1,:,:]
testRecord3 = data3[2,:,:]
train_lst3 = trainRecord3[:,0]
eval_lst3 = evalRecord3[:,0]
test_lst3 = testRecord3[:,0]


n2 = train_lst.shape[0]

plt.plot([i for i in range(0,n2)], train_lst, label="train")
plt.plot([i for i in range(0,n2)], eval_lst, label ="eval")
plt.plot([i for i in range(0,n2)], test_lst, label="test")
plt.xlabel('epoch')
plt.ylabel('loss')
plt.ylim((1500,3000))
plt.title('Loss_CIFAR_10_Hidden 120_epoch70_lr0.5e-3')
plt.legend()

plt.show()


plt.plot([i for i in range(0,n2)], train_lst, label="train_0.5e-3")
plt.plot([i for i in range(0,n2)], test_lst, label="test_0.5e-3")
plt.plot([i for i in range(0,n2)], train_lst2, label="train_1e-3")
plt.plot([i for i in range(0,n2)], test_lst2, label="test_1e-3")
# plt.plot([i for i in range(0,n2)], train_lst3, label="train_hidden200")
# plt.plot([i for i in range(0,n2)], test_lst3, label="test_hidden200")
plt.xlabel('epoch')
plt.ylabel('loss')
plt.title('Loss_CIFAR 10_diff learning rate (Hidden 120) ')
# plt.xlim((15,30))
plt.ylim((1500,3000))
plt.legend()

plt.show()