import torch
from matplotlib import pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

def latentcluster3D(model,data_loader, args):
    for batch_index, (data, labels) in enumerate(data_loader):
        data = data.to(args.device)
        labels = labels.to(args.device)
        z, mean, logvar, recon = model.forward(data,labels )
        z = z.data.cpu().numpy()
        if batch_index==0:
            latent = z
            value = labels.numpy()
        else:
            latent = np.vstack((latent,z))
            value = np.vstack((value,labels.numpy()))
        print(batch_index,z.shape, labels.numpy().shape,latent.shape,value.shape)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    surf=ax.scatter(latent[:, 0], latent[:, 1], latent[:,2],c = value.T.argmax(axis=0),cmap=plt.cm.Spectral)
    fig.colorbar(surf, shrink=0.5, aspect=10)
    plt.show()
    return

def latentcluster2D(model,data_loader, args):
    for batch_index, (data, labels) in enumerate(data_loader):
        data = data.to(args.device)
        labels = labels.to(args.device)
        z, mean, logvar, recon = model.forward(data,labels )
        z = z.data.cpu().numpy()
        if batch_index==0:
            latent = z
            value = labels.numpy()
        else:
            latent = np.vstack((latent,z))
            value = np.vstack((value,labels.numpy()))
        print(batch_index,z.shape, labels.numpy().shape,latent.shape,value.shape)
    plt.figure(figsize=(6, 6))
    plt.scatter(latent[:, 0], latent[:, 1], c = value.T.argmax(axis=0),cmap=plt.cm.Spectral)
    plt.colorbar()
    plt.show()
    # ax = fig.add_subplot(111, projection='3d')
    # surf=ax.scatter(latent[:, 0], latent[:, 1], latent[:,2],c = value.T.argmax(axis=0),cmap=plt.cm.Spectral)
    # fig.colorbar(surf, shrink=0.5, aspect=10)
    # plt.show()
    return
