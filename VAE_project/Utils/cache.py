import pickle
import datetime
import pathlib
def saveCache(lossRecord, args):
    time = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    cachePath = args.path + '/cache'
    pathlib.Path(cachePath).mkdir(parents=True,exist_ok=True)
    with open(cachePath + '/' + time +'.pickle', 'wb') as f:
        pickle.dump(lossRecord, f)
    with open(cachePath + '/' + time +'.txt', 'wb') as f:
        pickle.dump(str(args), f)
    return cachePath + '/' + time +'.pickle'

