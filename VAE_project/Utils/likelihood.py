import numpy as np
import torch
min_epsilon = 1e-5
max_epsilon = 1.-1e-5
def logsumexp(x):
    return np.log(np.sum(np.exp(x),axis=0))

def binary_cross_entropy(recon, x):
    probs = torch.clamp( recon, min=min_epsilon, max=max_epsilon )
    BCE = x*torch.log(probs) + (1.0-x)*torch.log(1.0-probs)
    return torch.sum(BCE,1)

#likelihood p(z) is normal distribution ~N(0,I)
# \int N(z;mean,var) log N(z;0,I) dz
def likelihood_log_p_z(z):
    p_z = - 0.5*torch.pow(z, 2)
    p_z = torch.sum(p_z, 1)
    return p_z

#likelihood q(z|x) given x is normal distribution ~N(mean, var)
# \int N(z;mean,var) log N(z;mean,var) dz
def likelihood_log_q_z(z, mean, logvar):
    q_z = - 0.5*(logvar + torch.pow(z - mean, 2)/torch.exp(logvar))
    q_z = torch.sum(q_z, 1)
    return q_z
    
