%download datasets from http://ufldl.stanford.edu/housenumbers/ 
load('train_32x32.mat');
X = X(:,:,:,1:10000);
X = reshape(X, 32*32*3, 10000);
X = double(X);
cov_mat = X*X'/size(X,2); %covariance matrix;
[U,I,V] = svd(cov_mat);
X1 = X(:,1);
%% 
U1 = U';
distance_rec = [];
for PCA_num = 200:50:1000
    para = U1(1:PCA_num,:)*X1;
    recon = (para'*U1(1:PCA_num,:))';
    diff = X1 - recon;
    distance = (diff'*diff)/(sqrt(X1'*X1)*sqrt(recon'*recon));
    distance_rec = [distance_rec, distance];
end
PCA_num = 200:50:1000;
plot(PCA_num,distance_rec);