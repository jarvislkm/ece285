from __future__ import print_function
import argparse
import torch
import numpy as np
import torch.utils.data
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image
import pathlib
target = 0
encodeSize = 200
saveInterval = 50
pathlib.Path('results/CIFAR-10/'+str(target)+'_'+str(encodeSize)).mkdir(parents=True, exist_ok=True)

parser = argparse.ArgumentParser(description='VAE MNIST Example')
parser.add_argument('--batch-size', type=int, default=128, metavar='N',
                    help='input batch size for training (default: 128)')
parser.add_argument('--epochs', type=int, default=100, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=50, metavar='N',
                    help='how many batches to wait before logging training status')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()


torch.manual_seed(args.seed)

device = torch.device("cuda" if args.cuda else "cpu")

kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
train_loader = torch.utils.data.DataLoader(
    datasets.CIFAR10('../data', train=True, download=True,
                   transform=transforms.ToTensor()),
    batch_size=args.batch_size, shuffle=True, **kwargs)
test_loader = torch.utils.data.DataLoader(
    datasets.CIFAR10('../data', train=False, transform=transforms.ToTensor()),
    batch_size=args.batch_size, shuffle=True, **kwargs)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

trainSet = []
testSet = []

for i, data in enumerate(train_loader, 0):
    inputs, labels = data
    for i in range(inputs.shape[0]):
        if labels[i] == target:
            image = torch.mean(inputs[i],0)
            trainSet.append(image)

for i, data in enumerate(test_loader, 0):
    inputs, labels = data
    for i in range(inputs.shape[0]):
        if labels[i] == target:
            image = torch.mean(inputs[i],0)
            testSet.append(image)

trainSet = trainSet[0: len(trainSet)-len(trainSet) % args.batch_size]
print(len(trainSet))
trainSet = torch.cat(trainSet) 
print(trainSet.shape)
trainSet = trainSet.view([-1, args.batch_size, 32, 32])
print(trainSet.shape)

testSet = testSet[0: len(testSet)-len(testSet) % args.batch_size]
print(len(testSet))
testSet = torch.cat(testSet) 
print(testSet.shape)
testSet = testSet.view([-1, args.batch_size, 32, 32])
print(testSet.shape)



class VAE(nn.Module):
    def __init__(self):
        super(VAE, self).__init__()

        self.fc1 = nn.Linear(1024, 800)
        self.fc21 = nn.Linear(800, encodeSize)
        self.fc22 = nn.Linear(800, encodeSize)
        self.fc3 = nn.Linear(encodeSize, 800)
        self.fc4 = nn.Linear(800, 1024)

    def encode(self, x):
        h1 = F.relu(self.fc1(x))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        if self.training:
            std = torch.exp(0.5*logvar)
            eps = torch.randn_like(std)
            return eps.mul(std).add_(mu)
        else:
            return mu

    def decode(self, z):
        h3 = F.relu(self.fc3(z))
        return F.sigmoid(self.fc4(h3))

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, 1024))
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar


model = VAE().to(device)
optimizer = optim.Adam(model.parameters(), lr=1e-3)


# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function(recon_x, x, mu, logvar):
    BCE = F.binary_cross_entropy(recon_x, x.view(-1, 1024), size_average=False)

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return BCE+KLD


def train(epoch):
    model.train()
    train_loss = 0
    for batch_idx, data in enumerate(trainSet):
        optimizer.zero_grad()
        data = data.to(device)
        recon_batch, mu, logvar = model(data)
        loss = loss_function(recon_batch, data, mu, logvar)
        loss.backward()
        train_loss += loss.item()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(trainSet)* len(data),
                100. * batch_idx / len(trainSet),
                loss.item() / len(data)))

    print('====> Epoch: {} Average loss: {:.4f}'.format(
          epoch, train_loss /(len(data)*len(trainSet)) ))


def test(epoch):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for i, data in enumerate(testSet):
            data = data.to(device)
            recon_batch, mu, logvar = model(data)
            test_loss += loss_function(recon_batch, data, mu, logvar).item()
            if epoch % saveInterval==0 and i == 0:
                n = min(data.size(0), 8)
                comparison = torch.cat([data[:n],
                                      recon_batch.view(args.batch_size, 32, 32)[:n]])
                save_image(comparison.view(n*2,1,32,32).cpu(),
                         'results/CIFAR-10/'+str(target)+'_'+str(encodeSize)+'/reconstruction_' + str(epoch) + '.png', nrow = n)

    test_loss /= (testSet.shape[0]*len(data))
    print('====> Test set loss: {:.4f}'.format(test_loss))

# St = torch.cat(trainSet[0:64])  
# save_image(St.view(64,1,32,32),
#             'results/CIFAR-10/'+str(target)+'/TARGET' + '.png')

for epoch in range(1, args.epochs + 1):
    train(epoch)
    test(epoch)
    with torch.no_grad():
        sample = torch.randn(64, encodeSize).to(device)
        sample = model.decode(sample).cpu()
        if epoch % saveInterval == 0:
            print(sample.shape)
            save_image(sample.view(64, 1, 32, 32),
            'results/CIFAR-10/'+str(target)+'_'+str(encodeSize)+'/sampleC_' + str(epoch) + '.png')

