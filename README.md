# README #

This README would normally document whatever steps are necessary to get your application up and running.

VAE_project contains code and results.

/Models contains all models

/Utils contains all function needed

/result contains running data

Things we achieved so far:
0. Theory of VAE
1. build different VAE models: vanilla VAE/ HVAE
2. calculate NLL of generated images using importance sampling
3. enable conditional VAE for specific tasks: number generation, style transfer

Report Arch:
1. Abstract (0.5)
2. Theory OF VAE (1)
	Figure 1, basic structure of vae;
3. Experiment
  1. model arch (3)
	1.1 vanilla vae / 2-layer vae
		Figure 2, arch of two model;
    1.2 cost function / sampling method explaination
	    basic generation result(figure/curve(cost/NLL), comparison different models)
		Lots of figure/curve  loss likelihood (random sampling street number)
  2. modifications (3)
	2.1 conditional vae, cvae showcase
	2.2 pca pre-processing, pca showcase
		Figure 3, modification of arch
  3. latent variable analysis / clustering result (1.5)
		Figure 4, analysis figure
4. summary and future work (0.5)
5. contribution / reference (0.5)


