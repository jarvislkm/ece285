from torchvision.utils import save_image
import torch
import pathlib
from Utils.loadData import pca_decode

def unisampling(number, model, epoch, args):
    savePath = args.path + '/' + 'unisampling'
    pathlib.Path(savePath).mkdir(parents=True,exist_ok=True)
    with torch.no_grad():
        sample = model.unisampling(number).to(args.device)
        if args.ifpca == True:
            decode = pca_decode(args.device, sample, args.mu, args.U)
        else:
            decode = sample     
        save_image(decode.view(number*number, args.inputSize[0], args.inputSize[1], args.inputSize[2]),
                   savePath+ '/' + str(epoch) + '.png',nrow=number)