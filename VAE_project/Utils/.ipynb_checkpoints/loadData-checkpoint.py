import torch
import torch.utils as utils
from torchvision import datasets, transforms
import numpy as np
from scipy.io import loadmat
import os
import pickle

def load_MNIST(args, **kwargs):
    args.inputSize = [1,28,28] #channel, W, H
    args.grey = True
    train_loader = utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True,
                         transform=transforms.ToTensor()),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    test_loader = utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.ToTensor()),
        batch_size=args.batch_size, shuffle=True, **kwargs)

    # shape should be [channel, w, h]
    train_x_raw = train_loader.dataset.train_data.float().numpy()/255
    train_x = np.reshape(train_x_raw, (train_x_raw.shape[0], train_x_raw.shape[1]* train_x_raw.shape[2]))
    test_x_raw = test_loader.dataset.test_data.float().numpy()/255
    test_x = np.reshape(test_x_raw, (test_x_raw.shape[0], test_x_raw.shape[1] * test_x_raw.shape[2]))

    if args.cvae_select == 'number':
        train_y = train_loader.dataset.train_labels.numpy()
        train_y = np.reshape(train_y, (train_y.shape[0],1))
        train_y_conditional = torch.zeros(train_x.shape[0], 10).scatter_(1, torch.tensor(train_y), 1)
        test_y = test_loader.dataset.test_labels.numpy()
        test_y = np.reshape(test_y, (test_y.shape[0],1))
        test_y_conditional = torch.zeros(test_x.shape[0], 10).scatter_(1, torch.tensor(test_y), 1)
    
    elif args.cvae_select == 'line':
        train_y_conditional = torch.tensor(train_x_raw[:,:, int(train_x_raw.shape[2]/2)])
        test_y_conditional = torch.tensor(test_x_raw[:,:, int(test_x_raw.shape[2]/2)])

    if args.cvae == True:
        args.conditional_dim = train_y_conditional.shape[1]

    val_x = train_x[55000:60000]
    val_y_conditional = train_y_conditional[55000:60000]
    train_x = train_x[0:55000]
    train_y_conditional = train_y_conditional[0:55000]

    if args.ifpca == True:
        mu, reduce_U, train_param, val_param, test_param = pca_encode(args, train_x, val_x, test_x)
        print(train_param[0])
    else:
        train_param = train_x
        val_param = val_x
        test_param = test_x
        mu = []
        reduce_U = []
    args.mu = mu
    args.U = reduce_U
    # PCA Test
    # decode_t = pca_decode(train_param, mu, reduce_U)
    # test_val = np.mean(decode_t[0]-train_x[0])
    # var = np.std(decode_t[0])
    # print(max(decode_t[0]-train_x[0]))
    
    # change to TensorDataset
    trainSet = utils.data.TensorDataset(torch.Tensor(train_param), train_y_conditional)
    train_loader = utils.data.DataLoader(trainSet,batch_size=args.batch_size, shuffle=True, **kwargs)

    testSet = utils.data.TensorDataset(torch.Tensor(test_param), test_y_conditional)
    test_loader = utils.data.DataLoader(testSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    valSet = utils.data.TensorDataset(torch.Tensor(val_param), val_y_conditional)
    val_loader = utils.data.DataLoader(valSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    return args, train_loader, val_loader, test_loader

def load_omniglot(args, **kwargs):
    args.inputSize = [1, 28 ,28]
    args.grey = True
    datapath = 'datasets/OMNIGLOT/chardata.mat'
    raw = loadmat(datapath)
    train_x = (raw['data'].T).astype('float32')
    test_x = (raw['testdata'].T).astype('float32')
    train_y = (raw['target'].T).astype('float32')
    test_y = (raw['testtarget'].T).astype('float32')

    val_x = train_x[22000:]
    train_x = train_x[:22000]
    val_y = train_y[22000:]
    train_y = train_y[:22000]

    if args.ifpca == True:
        mu, reduce_U, train_param, val_param, test_param = pca_encode(args, train_x, val_x, test_x)
        print(train_param[0])
    else:
        train_param = train_x
        val_param = val_x
        test_param = test_x
        mu = []
        reduce_U = []
    args.mu = mu
    args.U = reduce_U

    if args.cvae == True:
        args.conditional_dim = train_y.shape[1]

    trainSet = utils.data.TensorDataset(torch.Tensor(train_param), torch.Tensor(train_y))
    train_loader = utils.data.DataLoader(trainSet,batch_size=args.batch_size, shuffle=True, **kwargs)

    testSet = utils.data.TensorDataset(torch.Tensor(test_param), torch.Tensor(test_y))
    test_loader = utils.data.DataLoader(testSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    valSet = utils.data.TensorDataset(torch.Tensor(val_param), torch.Tensor(val_y))
    val_loader = utils.data.DataLoader(valSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    return args, train_loader, val_loader, test_loader

def load_CIFAR_10(args, **kwargs):
    if args.grey:
        args.inputSize = [1, 32 ,32]
    else:
        args.inputSize = [3, 32 ,32]

    train_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10('../data', train=True, download=True,
                    transform=transforms.ToTensor()),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10('../data', train=False, transform=transforms.ToTensor()),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
    target = 0 # 0:plane    

    train_x = train_loader.dataset.train_data/255
    train_y = train_loader.dataset.train_labels
    train_x = np.reshape(train_x, (train_x.shape[0], train_x.shape[1]* train_x.shape[2], train_x.shape[3])) 
    test_x = test_loader.dataset.test_data/255
    test_y = test_loader.dataset.test_labels
    test_x = np.reshape(test_x, (test_x.shape[0], test_x.shape[1] * test_x.shape[2], test_x.shape[3]))
    trainSet = []
    testSet = []
    i = 0
    while i < train_x.shape[0]:
        if train_y[i] == target:
            image = train_x[i]
            if args.grey == True:
                image = np.mean(train_x[i], 1)
            trainSet.append(image)
        i = i+1

    i = 0
    while i < test_x.shape[0]:
        if test_y[i] == target:
            image = test_x[i]
            if args.grey == True:
                image = np.mean(test_x[i], 1)
            testSet.append(image)
        i = i+1

    val_x = np.array(trainSet[4500:])
    train_x = np.array(trainSet[:4500])
    test_x = np.array(testSet)    

    if not args.grey:
        val_x = np.transpose(val_x, (0,2,1))
        train_x = np.transpose(train_x, (0,2,1))
        test_x = np.transpose(test_x, (0,2,1))
        val_x = np.reshape(val_x, (val_x.shape[0], val_x.shape[1]*val_x.shape[2]))
        train_x = np.reshape(train_x, (train_x.shape[0], train_x.shape[1]*train_x.shape[2]))
        test_x = np.reshape(test_x, (test_x.shape[0], test_x.shape[1]*test_x.shape[2]))

    if args.ifpca == True:
        mu, reduce_U, train_param, val_param, test_param = pca_encode(args, train_x, val_x, test_x)
        print(train_param[0])
    else:
        train_param = train_x
        val_param = val_x
        test_param = test_x
        mu = []
        reduce_U = []
    args.mu = mu
    args.U = reduce_U

    train_y_dummy = np.zeros((train_x.shape[0], 1))
    val_y_dummy = np.zeros((val_x.shape[0], 1))
    test_y_dummy = np.zeros((test_x.shape[0], 1))

    trainSet = utils.data.TensorDataset(torch.Tensor(train_param), torch.Tensor(train_y_dummy))
    train_loader = utils.data.DataLoader(trainSet,batch_size=args.batch_size, shuffle=True, **kwargs)

    testSet = utils.data.TensorDataset(torch.Tensor(test_param), torch.Tensor(test_y_dummy))
    test_loader = utils.data.DataLoader(testSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    valSet = utils.data.TensorDataset(torch.Tensor(val_param), torch.Tensor(val_y_dummy))
    val_loader = utils.data.DataLoader(valSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    return args, train_loader, val_loader, test_loader

def load_FreyFace(args, **kwargs):
    args.inputSize = [1, 28 ,20]
    args.grey = True
    datapath = 'datasets/FreyFace/ffdouble.mat'
    raw = loadmat(datapath)
    train_x = (raw['ffd'].T).astype('float32')
    # test_x = (raw['testdata'].T).astype('float32') # 没有test data

    np.random.shuffle(train_x)
    val_x = train_x[1500:1600]
    test_x = train_x[1600:]
    train_x = train_x[:1500]

    if args.ifpca == True:
        mu, reduce_U, train_param, val_param, test_param = pca_encode(args, train_x, val_x, test_x)
        print(train_param[0])
    else:
        train_param = train_x
        val_param = val_x
        test_param = test_x
        mu = []
        reduce_U = []
    args.mu = mu
    args.U = reduce_U

    train_y_dummy = np.zeros((train_x.shape[0], 1))
    val_y_dummy = np.zeros((val_x.shape[0], 1))
    test_y_dummy = np.zeros((test_x.shape[0], 1))

    trainSet = utils.data.TensorDataset(torch.Tensor(train_param), torch.Tensor(train_y_dummy))
    train_loader = utils.data.DataLoader(trainSet,batch_size=args.batch_size, shuffle=True, **kwargs)

    testSet = utils.data.TensorDataset(torch.Tensor(test_param), torch.Tensor(test_y_dummy))
    test_loader = utils.data.DataLoader(testSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    valSet = utils.data.TensorDataset(torch.Tensor(val_param), torch.Tensor(val_y_dummy))
    val_loader = utils.data.DataLoader(valSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    return args, train_loader, val_loader, test_loader

def load_StreetNumber(args, **kwargs):
    if args.grey:
        args.inputSize = [1, 32 ,32]
    else:
        args.inputSize = [3, 32 ,32]

    datapath_train = 'datasets/streetnumber/train_32x32.mat'
    datapath_test = 'datasets/streetnumber/test_32x32.mat'
    raw_train = loadmat(datapath_train)
    raw_test = loadmat(datapath_test)
    train_x_raw = (raw_train['X']).astype('float32')/255
    test_x_raw = (raw_test['X']).astype('float32')/255
    train_x_raw = np.transpose(np.array(train_x_raw),(2,0,1,3))
    test_x_raw = np.transpose(np.array(test_x_raw),(2,0,1,3)) # channel, w, h, num

    if args.grey:
        train_x_raw = np.mean(train_x_raw, axis=0) # w, h, num
        train_x = np.reshape(train_x_raw, (32*32, -1))
        test_x_raw = np.mean(test_x_raw, axis=0) # w, h, num
        test_x = np.reshape(test_x_raw, (32*32, -1))
    else:
        train_x = np.reshape(train_x_raw, (32*32*3, -1))
        test_x = np.reshape(test_x_raw, (32*32*3, -1))

    train_x = np.transpose(train_x, (1,0))
    test_x = np.transpose(test_x, (1,0))

    if args.cvae_select == 'number':
        train_y = raw_train['y'].astype('int')
        train_y = np.reshape(train_y, (train_y.shape[0],1))
        train_y_conditional = torch.zeros(train_x.shape[0], 10).scatter_(1, torch.tensor(train_y).long(), 1)
        test_y = raw_test['y'].astype('int')
        test_y = np.reshape(test_y, (test_y.shape[0],1))
        test_y_conditional = torch.zeros(test_x.shape[0], 10).scatter_(1, torch.tensor(test_y).long(), 1)

    elif args.cvae_select == 'line':
        if args.grey:
            train_y_conditional = train_x_raw[:,int(train_x_raw.shape[1]/2),:]
            train_y_conditional = np.transpose(train_y_conditional, (1,0))
            test_y_conditional = test_x_raw[:,int(test_x_raw.shape[1]/2),:]
            test_y_conditional = np.transpose(test_y_conditional, (1,0))
        else:
            train_y_conditional = train_x_raw[:,:,int(train_x_raw.shape[2]/2),:]
            train_y_conditional = np.reshape(train_y_conditional, (train_y_conditional.shape[0]*train_y_conditional.shape[1], -1))
            train_y_conditional = np.transpose(train_y_conditional, (1,0))
            test_y_conditional = test_x_raw[:,:,int(test_x_raw.shape[2]/2),:]
            test_y_conditional = np.reshape(test_y_conditional, (test_y_conditional.shape[0]*test_y_conditional.shape[1], -1))
            test_y_conditional = np.transpose(test_y_conditional, (1,0))

    if args.cvae == True:
        args.conditional_dim = train_y_conditional.shape[1]

    val_x = train_x[70000:]
    val_y_conditional = train_y_conditional[70000:]
    train_x = train_x[:70000]
    train_y_conditional = train_y_conditional[:70000]

    if args.ifpca == True:
        mu, reduce_U, train_param, val_param, test_param = pca_encode(args, train_x, val_x, test_x)
        print(train_param[0])
    else:
        train_param = train_x
        val_param = val_x
        test_param = test_x
        mu = []
        reduce_U = []
    args.mu = mu
    args.U = reduce_U

    trainSet = utils.data.TensorDataset(torch.Tensor(train_param), torch.Tensor(train_y_conditional))
    train_loader = utils.data.DataLoader(trainSet,batch_size=args.batch_size, shuffle=True, **kwargs)

    testSet = utils.data.TensorDataset(torch.Tensor(test_param), torch.Tensor(test_y_conditional))
    test_loader = utils.data.DataLoader(testSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    valSet = utils.data.TensorDataset(torch.Tensor(val_param), torch.Tensor(val_y_conditional))
    val_loader = utils.data.DataLoader(valSet, batch_size=args.batch_size, shuffle=True, **kwargs)

    return args, train_loader, val_loader, test_loader

def load_data(args, **kwargs):
    if args.dataSet == 'MNIST':
        args, train_loader, val_loader, test_loader = load_MNIST(args, **kwargs)
    elif args.dataSet == 'OMNIGLOT':
        args, train_loader, val_loader, test_loader = load_omniglot(args, **kwargs)
    elif args.dataSet == 'CIFAR_10':
        args, train_loader, val_loader, test_loader = load_CIFAR_10(args, **kwargs)
    elif args.dataSet == 'FreyFace':
        args, train_loader, val_loader, test_loader = load_FreyFace(args, **kwargs)
    elif args.dataSet == 'StreetNumber':
        args, train_loader, val_loader, test_loader = load_StreetNumber(args, **kwargs)
    return args, train_loader, val_loader, test_loader

# def dataNormalize(train_loader):
#     n = train_loader.shape[1]
#     m = train_loader.shape[0]
#     mu = np.mean(train_loader,axis=0)
#     for i in range(n):
#         train_loader[:,i] = train_loader[:,i] - mu[i]
#     return train_loader, mu

def pca_basis(args, train_loader_norm):
    m = train_loader_norm.shape[0]
    n = train_loader_norm.shape[1]        
    # Sigma: covariance matrix
    Sigma = np.dot(np.transpose(train_loader_norm),train_loader_norm)
    # print(Sigma[0])
    U,S,V = np.linalg.svd(Sigma)
    K = min(args.pcadimension, n)
    reduce_U = U[:,0:K]
    print(reduce_U.shape)
    return reduce_U

def pca_encode(args, train_loader, val_loader, test_loader):
    train_loader_norm = train_loader
    mu = 0.0
    val_loader_norm = val_loader - mu
    test_loader_norm = test_loader - mu
    reduce_U = pca_basis(args, train_loader)
    train_param = np.dot(train_loader_norm, reduce_U)
    val_param = np.dot(val_loader_norm, reduce_U)
    test_param = np.dot(test_loader_norm, reduce_U)
    return mu, reduce_U, train_param, val_param, test_param

def pca_decode(device, signal, mu, U):
    decode = torch.mm(signal, torch.Tensor(np.transpose(U)).to(device))
    decode = decode + torch.tensor(mu).to(device)
    return decode